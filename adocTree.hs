import Control.Monad (forM)
import System.Directory (doesDirectoryExist, getDirectoryContents)
import System.FilePath ((</>), splitDirectories)
import qualified Data.List as L
import qualified System.IO
import qualified Data.Tree as Tree


type FileWithContent = (FilePath, String)
type FileIncludes = (FilePath, [String])


getRecursiveContents :: FilePath -> IO [FilePath]
getRecursiveContents topdir = do
    names <- getDirectoryContents topdir
    let properNames = filter (`notElem` [".", ".."]) names
    paths <- forM properNames $ \name -> do
        let path = topdir </> name
        isDirectory <- doesDirectoryExist path
        if isDirectory
            then getRecursiveContents path
            else return [path]
    return (concat paths)

getFileWithContent :: FilePath -> IO FileWithContent
getFileWithContent filename = do
    content <- readFile filename
    return (filename, content)

isAdoc :: FilePath -> Bool
isAdoc filepath
    | L.isSuffixOf ".adoc" filepath     = True
    | L.isSuffixOf ".asciidoc" filepath = True
    | otherwise                         = False

fileFromInclude :: String -> String -> String
fileFromInclude includingF includeSt = leadingDir </> (takeWhile (/= '[') afterPrefix)
    where afterPrefix = case (L.stripPrefix "include::" includeSt) of
                            Just s -> s
                            Nothing -> error "Include statement does not start with 'include::'"
          leadingDir = foldl1 (</>) $ init $ splitDirectories includingF

showIncludes :: FileIncludes -> String
showIncludes (f, c) = f ++ "\n" ++ (unlines $ map ("    " ++) c)

getFilesWithIncludes :: [FileWithContent] -> [FileIncludes]
getFilesWithIncludes files = map (\(file, content) -> (file, getIncludes file content)) files
    where getIncludes f s = map (fileFromInclude f) $ filter (L.isPrefixOf "include::") $ lines s

notIncluded :: [FileIncludes] -> [FileIncludes]
notIncluded fileIncludes = filter (\(f, i) -> not (elem f includeSts)) fileIncludes
    where includeSts = concat $ map snd fileIncludes


main = do
    paths <- getRecursiveContents "."
    let adocfiles = filter isAdoc paths
    adocsWithContent <- mapM getFileWithContent adocfiles

    let includes = getFilesWithIncludes adocsWithContent
    let includesNotIncluded = notIncluded includes

    -- mapM putStrLn (map showIncludes includes)

    -- Data.Tree
    -- let buildNode x = if 2*x + 1 > 7 then (x, []) else (x, [2*x, 2*x+1])
    let buildNode filename = case lookup filename includes of
                                  Just [] -> (filename, [])
                                  -- Nothing -> error ("File name in list is not in list: " ++ filename)
                                  Nothing -> ("[File not found] " ++ filename, [])
                                  Just l -> (filename, l)
    let printTree n = putStr $ (++ "\n") $ Tree.drawTree $ Tree.unfoldTree buildNode n
    mapM printTree (map fst includesNotIncluded)

    putStrLn "Modules not included in any found file:"
    print (map fst includesNotIncluded)

